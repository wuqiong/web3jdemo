import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.tools.javac.util.StringUtils;
import org.web3j.crypto.*;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.request.Transaction;
import org.web3j.protocol.core.methods.response.EthGetTransactionCount;
import org.web3j.protocol.core.methods.response.EthSendTransaction;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.protocol.http.HttpService;
import org.web3j.tx.Transfer;
import org.web3j.utils.Convert;
import org.web3j.utils.Numeric;
import org.web3j.utils.Strings;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;

import static org.web3j.crypto.Keys.createEcKeyPair;

public class Main {


    public static void main(String[] args) {
        try {

//            genWallet("1234");
//
//            getPrivateKey("1234", "./keystore");

            translate("1234", "./keystore", "t06623963639de631e9f07e358fbd422cbd5d26523", new BigInteger("100"), "test");

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    //生成地址
    public static void genWallet(String password) throws Exception {
        ECKeyPair ecKeyPair = createEcKeyPair();
        WalletFile walletFile = Wallet.createLight(password, ecKeyPair);

        //!!!展示给客户的地址，务必调用format2TTCAddress()进行格式化
        System.out.println("address:" + format2TTCAddress(walletFile.getAddress()));
        System.out.println("privateKey:" + Wallet.decrypt(password, walletFile).getPrivateKey().toString(16));
        System.out.println("publicKey:" + Wallet.decrypt(password, walletFile).getPublicKey().toString(16));


        //将walletFile存入本地
        ObjectMapper mapper = new ObjectMapper();
        String keystore = mapper.writeValueAsString(walletFile);
        FileWriter keystoreFile = new FileWriter("keystore", false);
        keystoreFile.write(keystore);
        keystoreFile.flush();
    }


    //根据keystore导出地址，公钥，私钥
    public static void getPrivateKey(String pwd, String keyStoreFilePath) throws Exception {
        Credentials credentials = WalletUtils.loadCredentials(pwd, keyStoreFilePath);

        String address = credentials.getAddress();   //钱包地址
        ECKeyPair ecKeyPair = credentials.getEcKeyPair();
        String priKey = ecKeyPair.getPrivateKey().toString(16);  //私钥
        String pubKey = ecKeyPair.getPublicKey().toString(16);   //公钥

        System.out.println("address:" + format2TTCAddress(address));
        System.out.println("privateKey:" + priKey);
        System.out.println("publicKey:" + pubKey);
    }


    /**
     * 构造交易，离线签名交易，广播
     *
     * @param pwd
     * @param keyStoreFilePath
     * @param transValue       转账金额
     * @param note             备注
     */
    public static void translate(String pwd, String keyStoreFilePath, String to, BigInteger transValue, String note) {
        try {

            //建立对应节点http-RPC对象
            Web3j web3j = Web3j.build(new HttpService("http://127.0.0.1:8891"));  //使用时改为链的rpc地址

            //加载keystore,也可以通过私钥
            Credentials credentials = WalletUtils.loadCredentials(pwd, keyStoreFilePath);


            String fromAddress = credentials.getAddress();  //获取地址
            String data = toHex(note);   //将备注转为十六进制

            //地址校验
            to = format2EthAddress(to);
            if (!WalletUtils.isValidAddress(to)) {
                System.out.println("toAddress is wrong");
            }

            EthGetTransactionCount ethGetTransactionCount = web3j
                    .ethGetTransactionCount(fromAddress, DefaultBlockParameterName.LATEST).send();
            BigInteger nonce = ethGetTransactionCount.getTransactionCount();

            BigInteger gasPrice = web3j.ethGasPrice().send().getGasPrice();   //从链上获取gasPrice，也可以自己设定某个值
            BigInteger gasLimit = BigInteger.ONE;   //
            Transaction trans = new Transaction(fromAddress, nonce, gasPrice, gasLimit, to,
                    transValue, data);
            gasLimit = web3j.ethEstimateGas(trans).send().getAmountUsed();   //从链上获取gasLimit

            RawTransaction rawTransaction = RawTransaction.createTransaction(nonce, gasPrice, gasLimit, to, transValue, data);

            // 离线签名及广播
            byte[] signedMessage = TransactionEncoder.signMessage(rawTransaction, credentials);
            String hexValue = Numeric.toHexString(signedMessage);
            EthSendTransaction ethSendTransaction = web3j.ethSendRawTransaction(hexValue).send();

            if (ethSendTransaction != null) {

                if (!ethSendTransaction.hasError()) {  //成功
                    String txHashLocal = Hash.sha3(hexValue);
                    String txHashRemote = ethSendTransaction.getTransactionHash();
                    System.out.println("txHashRemote--->" + txHashRemote);
                } else {  //失败
                    System.out.println("ethSendTransaction Err: " + ethSendTransaction.getError().getMessage());
                }
            } else {
                System.out.println("ethSendTransaction Err: ethSendTransaction is null");
            }
        } catch (Exception e) {
            System.out.println("Err Exception: " + e.getMessage());
            e.printStackTrace();
        }
    }


    public static String toHex(String arg) {
        return String.format("%040x", new BigInteger(1, arg.getBytes(/*YOUR_CHARSET?*/)));
    }

    //转账时，将ttc地址转为eth地址
    public static String format2EthAddress(String address) {
        if (address != null) {
            if (address.startsWith("t0")) {
                address = "0x".concat(address.substring(2));
            }
        }
        return address;
    }

    //展示时，将eth地址转为ttc地址
    public static String format2TTCAddress(String address){
        if (address != null) {
            if (address.startsWith("0x")) {
                address = "t0".concat(address.substring(2));
            }
        }
        return address;
    }

}


